#!/bin/sh
echo "Building site...\n"

hugo

echo "Copying artifacts to web server...\n"
scp -r public/* merry:/var/www/weightofdreams.com/

echo "Done.\n"

