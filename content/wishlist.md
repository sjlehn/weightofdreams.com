---
title: "Wish List"
date: 2020-11-15
draft: false
menu: main
---

## The Family

Stuff in this category includes larger items that we're looking for. If you happen across them on a garage or estate sale or something, please consider calling us and asking us about it.

 * Commercial meat grinder
 * Commercial meat slicer (Hobart or similar)

---

## Me

Please avoid plastics if at all possible, and if you're getting me clothes, consider organic or secondhand. Thanks!

ok, ok, I give in (for now). By popular demand, I have an Amazon wish list, so you can mark things as purchased now - https://www.amazon.com/hz/wishlist/ls/3QKD2LTI7GF89

---
