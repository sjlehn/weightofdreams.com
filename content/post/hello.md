---
title: "Hello"
date: 2018-12-11T22:03:44-06:00
draft: false
---
Hi there.  This is a blog, though time will tell if I find time to write much here.

Perhaps you're wondering about the title...it's taken from the lyrics of a Kutless song off the album Music Inspired by the Chronicles of Narnia called 'More Than it Seems':

{{< youtube 2Cl1TTW-m0g >}}

I've always liked that song.  "Show me the weight of these dreams" - we don't really know where our dreams will take us, though perhaps it is better that we don't know.  St. John Baptiste de la Salle said if he'd known where the path of his founding the Christian Brothers would ultimately lead, he likely never would have taken it.