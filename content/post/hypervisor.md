---
title: "Hypervisor"
date: 2020-03-19T20:44:45-05:00
draft: true
---

# Getting started with a hypervisor

Base install is a minimal Debian 10 (Buster) image

 - don't forget your root password! (passwordstore.org)
 - install sudo
 - add your basic user to the sudo group (`adduser sjlehn sudo`)
   - if you're logged in as that user, log out and log back in to get the group change to take effect
 - install the manpage viewer if it's missing (apt install man-db)
 - install the basic KVM packages
 <code>
 sudo apt install qemu-kvm libvirt-clients libvirt-daemon-system bridge-utils libguestfs-tools genisoimage virtinst libosinfo-bin
 </code>
 - Add your user to groups:
 <code>
$ sudo adduser vivek libvirt
$ sudo adduser vivek libvirt-qemu
$ newgrp libvirt
$ newgrp libvirt-qemu
</code>

 


# Resources
https://mike42.me/blog/2019-08-how-to-use-the-qemu-bridge-helper-on-debian-10
https://www.cyberciti.biz/faq/install-kvm-server-debian-linux-9-headless-server/